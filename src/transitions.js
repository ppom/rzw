export function crop(node, {
    delay = 0,
    duration = 300
}) {
    const h = getComputedStyle(node).height.replace(/[^0-9]/g, '');

    return {
        delay,
        duration,
        css: t => `transform: scaleY(${t}); height: ${t * h}px`
    };
}
