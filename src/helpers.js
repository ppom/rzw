// DATE helpers

export const oneSecond = 1000;
export const oneMinute = 60 * oneSecond;
export const oneHour = 60 * oneMinute;
export const oneDay = 24 * oneHour;
export const oneWeek = 7 * oneDay;

const months = [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
export const monthToString = number => months[number];

export const isSameDay = (day1, day2) => day1.getFullYear() === day2.getFullYear()
    && day1.getMonth() === day2.getMonth()
    && day1.getDate() === day2.getDate();

export const isNextDay = (day1, day2) => isSameDay(new Date(day1.getTime() + oneDay), day2);

export const diffDays = (day1, day2) => Math.abs(Math.floor(
    (Date.UTC(day2.getFullYear(), day2.getMonth(), day2.getDate()) - 
     Date.UTC(day1.getFullYear(), day1.getMonth(), day1.getDate()) ) / oneDay));

export const isLater = (day1, day2) => day1.getTime() > day2.getTime();

export const toString = day => day.toISOString().substr(0, 10);

export const parity = day => (day.getDay() + day.getMonth()) % 2;

// COLOR helper

export const isHexColor = hexColor => /[0-9A-Fa-f]{6}/g.test(hexColor) && hexColor.length === 6;
const colorLuminance = (hex = '000000', lum = 0) => {
    hex = hex.substring(0,1) === '#' ? hex.substring(1): hex;
    var rgb = '#', c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i*2,2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ('00'+c).substr(c.length);
    }
    return rgb;
};
export const darken   = hex => colorLuminance(hex, -0.5);
export const hovered  = hex => colorLuminance(hex, 0.5);
export const getColor = color => color ? `#${color}` : '#891111';

