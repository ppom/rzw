import { writable, derived } from 'svelte/store';
import { isHexColor, hovered, darken } from './helpers';

// STORES

const daysFactory = () => {
	const { subscribe, update, set } = writable({});
	return {
		set,
		subscribe,
		modifyDay: day => {
			update(days => ({
				...days,
				[day.date]: day
			}));
			save_data(day);
		}
	};
};

export const popin    = writable({});
export const calendar = writable({});
export const user     = writable({});
export const music    = writable([]);

export const days = daysFactory();

let token, session_id;
const docStyle = document.documentElement.style;

user.subscribe(value => {
	token = value.token;
	session_id = value.session_id;
	// Update CSS colors
	const bloodOut = `#${value.color || '891111'}`;
	docStyle.setProperty("--blood-out", bloodOut);
	docStyle.setProperty("--blood-hover", hovered(bloodOut));
	docStyle.setProperty("--nav-bar", darken(bloodOut));
});

const unsubscribers = [
	() => {
		calendar.set({});
		popin.set({});
		user.set({});
		days.set({});
	}
];

// API

const api = async (data, callback) => {
	const request =  new Request('/api.php', {
		method : 'POST',
		credentials: 'same-origin',
		body: JSON.stringify({ ...data, token}),
		headers: {
			'Content-Type': 'application/json',
			'Cookie': `PHPSESSID=${session_id}`
		}
	});

	return fetch(request)
		.then(data => {
			return data.text();
		})
		.then(text => {
			let json;
			try {
				json = JSON.parse(text);
			} catch(error) {
				json = {};
			}
			return callback(json);
		});
};

const check_connect = data => {
	if(data.code == 401) {
		unsubscribers.forEach(fn => fn());
		return false;
	}
	return true;
};

export const connect = async (mail, password) => await api(
	{
		action: 'connect',
		mail,
		passwd: password
	},
	({
		code,
		token,
		session_id,
		autoplay,
		color
	}) => {
		if(code == 200) {
			user.set({
				mail,
				token,
				session_id,
				autoplay: autoplay === '1',
				color: isHexColor(color) ? color : '891111'
			});
			calendar.set({ weeksBefore: 8, weeksAfter: 5 });
			setTimeout(get_data, 0);
		}
		return code;
	}
);

export const disconnect = () => api(
	{ action: 'disconnect' },
	() => unsubscribers.forEach(fn => fn())
);

export const save_data = ({
	date,
	flow,
	pain,
	text
}) => api(
	{
		action: 'set_day',
		date,
		flow,
		pain,
		text
	},
	data => check_connect(data)
);

export const get_data = () => api(
	{
		action: 'get_days'
	},
	data => {
		if(check_connect(data)) {
			if(data.music) {
				music.set([ ...data.music ]);
				delete(data.music);
			}
			days.set(Object.values(data).reduce((days, day) => day.date ? { ...days, [day.date]: day } : days));
		}
	}
);

export const change_param = param => api(
	{
		action: 'change_param',
		...param
	},
	data => check_connect(data)
);
