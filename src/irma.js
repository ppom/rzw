import { derived } from 'svelte/store';
import { days } from './api.js';
import { isNextDay, diffDays, oneDay, toString } from './helpers.js';

const days_to_periods = days => {
    if(!days) return false;
    const blood = Object.values(days).filter(day => day.flow > 0);
    const objs = blood.map(day => new Date(day.date)).sort((d1, d2) => d1.getTime() - d2.getTime());
    let i = 0, j = 0;
    let intervals = [];
    while(i<objs.length) {
        j = i;
        while(j+1 < objs.length && isNextDay(objs[j], objs[j+1])) j++;
        intervals.push([ objs[i], objs[j] ]);
        i = j+1;
    }
    return intervals;
};

// "Irma la voyante" : predict next period
export const next = derived(days,
    $days => {
        try {
            const periods = days_to_periods($days);

            if(!periods.length) return undefined;

            const periodRaw = periods.map(elt => elt[0])
                .map((cur, i, arr) => i ? diffDays(cur, arr[i-1]) : 0)
                .slice(1);
            const periodStat = periodRaw.reduce(([ len, sum ], cur) => ([ len+1, sum+cur]), [0, 0]);
            const periodMean = periodStat[1] / periodStat[0];

            const bloodRaw = periods.map(([ begin, end ]) => diffDays(begin, end));
            const bloodStat = bloodRaw.reduce(([ len, sum ], cur) => ([ len+1, sum+cur]), [0, 0]);
            const bloodMean = 1 + bloodStat[1] / bloodStat[0];

            const nextBegin = new Date(periods[periods.length-1][0].getTime() + periodMean * oneDay);
            const nextBegins = Array.from({length: Math.round(bloodMean)}, (v, i) => i)
                .map(i => toString(new Date(nextBegin.getTime() + i * oneDay)))
                .reduce((acc, cur) => ({ ...acc, [cur]: true}), {});

            return nextBegins;
        } catch (e) {
            return undefined;
        }
    }
);

