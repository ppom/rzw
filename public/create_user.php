<?php
$ok = false;
$web_alert = "";
$log_alert = "";
$bdd = 0;

if (!isset($_POST['mail'])) {
	$web_alert = "no username specified";
} else if (!isset($_POST['passwd'])) {
	$web_alert = "no user password";
} else if (!isset($_POST['mypasswd'])) {
	$web_alert = "no admin password";
} else if (getenv('RZW_ADMIN_PASSWORD_FILE') == "") {
	$log_alert = "no path to admin password";
	$web_alert = "internal server error";
} else if (!is_file(getenv('RZW_ADMIN_PASSWORD_FILE'))) {
	$log_alert = "no file on the specified admin password path";
	$web_alert = "internal server error";
} else if ($_POST['mypasswd'] != trim(file_get_contents(getenv('RZW_ADMIN_PASSWORD_FILE')))) {
	$web_alert = "incorrect admin password";
	$log_alert = "incorrect admin password";
} else {
	$ok = true;
}

if ($ok) {
	try { $bdd = new PDO('sqlite:' . getenv('RZW_DB_FILE')); }
	catch (PDOException $e) { die('Connection failed: ' . $e->getMessage()); }
	$mail = $_POST['mail'];
	$passwd = password_hash($_POST['passwd'], PASSWORD_DEFAULT);

	$requete = $bdd->prepare(
		'INSERT INTO user (mail, passwd, autoplay, color) VALUES (?, ?, 1, \'891111\')'
	);
	if($requete->execute(array($mail, $passwd))) {
		$web_alert = "$mail créé !";
		$log_alert = "$mail créé !";
	} else {
		$web_alert = "internal server error";
		$log_alert = "database error...";
	}
}

if ($log_alert != "") {
	# Log errors, for debugging and fail2ban
	error_log($_SERVER ['REMOTE_ADDR'] . ';create_user;' . $log_alert);
}
?>
<html>
<body>
<?php echo $web_alert ?>
<form
	action="create_user.php"
	method="post"
>
	name:<input id="mail" name="mail" type="text"/><br/>
	passwd:<input id="passwd" name="passwd" type="password"/><br/>
	admin passwd:<input id="mypasswd" name="mypasswd" type="password"/><br/>
	<input type="submit" />
</form>
</body>
</html>
