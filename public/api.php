<?php
// Définitions de fonctions
function initConnexion() {
	try { return new PDO('sqlite:' . getenv('RZW_DB_FILE')); }
		catch (PDOException $e) { die('Connection failed: ' . $e->getMessage()); }
}

function maj_token() { $_SESSION['token_time'] = time(); }
function make_token() { $_SESSION['token'] = uniqid(rand(), true); maj_token(); }

function verifier_token($token) {
	if(isset($_SESSION['token'])) {
		if(isset($_SESSION['token_time'])) {
			if($_SESSION['token'] == $token) {
				if($_SESSION['token_time'] >= time()-60*5) {
					maj_token();
					return true;
	}	}	}	}
	return false;
}
function supprimer_token() {
	if(isset($_SESSION['token']))      unset($_SESSION['token']);
	if(isset($_SESSION['token_time'])) unset($_SESSION['token_time']);
}

function send($data_object) { echo json_encode($data_object); }
function send_bad_request() { send(array('code' => 400)); }
function send_unauthorized() { send(array('code' => 401)); }
function send_too_much_requests() { send(array('code' => 429)); }
function send_success() { send(array('code' => 200)); }

function connect($bdd, $json) {
	if (!isset($json['mail']) || !isset($json['passwd'])) {
		send_bad_request();
		return false;
	}
	# On a toutes les infos nécessaires : mail, passwd
	$requete = $bdd->prepare('SELECT id, mail, passwd, color, autoplay FROM user WHERE mail = ?');
	$requete->execute(array($json['mail']));
	$resultat = $requete->fetch(PDO::FETCH_ASSOC);

	if (!$resultat || !$resultat['id']) {
		error_log($_SERVER ['REMOTE_ADDR'] . ';login;account does not exist');
		send_unauthorized(); # On ne dit pas au client si le compte existe
		return false;
	}
	# On a bien un compte associé à ce mail
	if (!password_verify($json['passwd'], $resultat['passwd'])) {
		error_log($_SERVER ['REMOTE_ADDR'] . ';login;incorrect password');
		send_unauthorized(); # On ne dit pas au client si le compte existe
		return false;
	}
	# Le mot de passe est correct
	$_SESSION['mail'] = $resultat['mail'];
	$_SESSION['id'] = $resultat['id'];
	make_token();
	send(array(
		'code' => 200,
		'token' => $_SESSION['token'],
		'session_id' => session_id(),
		'color' => $resultat['color'],
		'autoplay' => $resultat['autoplay']
	));
}
function disconnect($bdd, $json) {
	foreach ($_SESSION as $key => $value) {
		unset($_SESSION[$key]);
	}
	session_destroy();
	send_success();
}
function get_days($bdd, $json) {
	if(!verifier_token($json['token'])) {
	send_unauthorized();
		return false;
	}
	$requete = $bdd->prepare('SELECT date, flow, pain, text FROM day WHERE user_id = ?');
	$requete->execute(array($_SESSION['id']));
	$resultat = $requete->fetchAll(PDO::FETCH_ASSOC);
	# Add entries to object
	foreach ($resultat as $value) {
		$data_object[$value['date']] = $value;
	}
	# Set error code
	$data_object['code'] = 200;

	# Add music files to object
	$dir = scandir('music');
	$music = array();
	foreach ($dir as $value) {
		if(! in_array($value, array(".", "..", "index.php"))) {
			$music[] = $value;
		}
	}
	shuffle($music);
	$data_object['music'] = $music;

	send($data_object);
}
function set_day($bdd, $json) {
	if(!verifier_token($json['token'])) {
	send_unauthorized();
		return false;
	}
	if(!isset($json['date']) || !isset($json['flow']) || !isset($json['text']) || !isset($json['pain'])
		|| !preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $json['date'])
		|| $json['flow'] < 0 || $json['flow'] > 100 || $json['pain'] < 0 || $json['pain'] > 1) {
		send_bad_request();
		return false;
	}
	# On supprime la ligne si elle existe
	$requete = $bdd->prepare('DELETE FROM day WHERE date = ? AND user_id = ?');
	$requete->execute(array($json['date'], $_SESSION['id']));

	if($json['flow'] != 0 || $json['pain'] != 0 || strlen($json['text']) > 0) {
		# Si on a bien flux et/ou douleur et/ou commentaire
		# On insère la nouvelle ligne
		$requete = $bdd->prepare('INSERT INTO day (date, user_id, flow, pain, text) VALUES (?, ?, ?, ?, ?)');
		$requete->execute(array($json['date'], $_SESSION['id'], $json['flow'], $json['pain'], $json['text']));
	}
	send_success();
}
function change_param($bdd, $json) {
	if(!verifier_token($json['token'])) {
		send_unauthorized();
		return false;
	}
	$value;
	$request;
	if(isset($json['passwd']) && preg_match("/*{8}/", $json['passwd'])) {
		$value = password_hash($json['passwd'], PASSWORD_DEFAULT);
		$request = $bdd->prepare('UPDATE user SET passwd = ? WHERE id = ?');
	}elseif(isset($json['color']) && preg_match("/[0-9a-zA-Z]{6}/", $json['color'])) {
		$value = $json['color'];
		$request = $bdd->prepare('UPDATE user SET color = ? WHERE id = ?');
	}elseif(isset($json['autoplay'])) {
		$value = $json['autoplay'];
		echo gettype($value);
		$request = $bdd->prepare('UPDATE user SET autoplay = ? WHERE id = ?');
	}else{
		send_bad_request();
		return false;
	}
	$request->execute(array($value, $_SESSION['id']));
	send_success();
}
// Fin des définitions


// créer session (ne fait rien si existe)
session_start();
// connexion à la BD
$bdd = initConnexion();

$json = json_decode(file_get_contents('php://input'), true);
$action = $json['action'];

//switch des fonctions à executer
switch($action) {
    case 'connect':
    case 'disconnect':
    case 'get_days':
    case 'set_day':
    case 'change_param':
	$action($bdd, $json);
	break;
    default:
	send_bad_request();
}
