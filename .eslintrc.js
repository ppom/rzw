module.exports = {
    env: {
        browser: true,
        es6: true
    },
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly"
    },
    parserOptions: {
        ecmaVersion: 2019,
        sourceType: "module",
    },
    plugins: [
      "svelte3"
    ],
    overrides: [
      {
        files: [
          "**/*.svelte"
        ],
        processor: "svelte3/svelte3"
      }
    ],
    /* extends: "eslint:recommended", */
    rules: {
      indent: [
        "error",
        4
      ],
      'linebreak-style': [
        "error",
        "unix"
      ],
      quotes: [
        "error",
        "single"
      ],
      semi: [
        "error",
        "always"
      ]
    }
};
