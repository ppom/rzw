-------------------------------------
-- SQLite
-- script to create the rzw database
-- created  2019-07-15
-- modified 2020-04-05
-- modified 2021-05-24 to make the script idempotent (will not harm an existing db)
-- by ppom
-------------------------------------

CREATE TABLE IF NOT EXISTS user (
	id INTEGER PRIMARY KEY,
	mail TEXT NOT NULL UNIQUE,
	passwd TEXT NOT NULL,
	color TEXT,
	autoplay INTEGER
);

CREATE TABLE IF NOT EXISTS day (
	date TEXT NOT NULL,
	user_id INTEGER NOT NULL REFERENCES user(id),
	flow INTEGER,
	pain INTEGER,
	text TEXT,
	PRIMARY KEY (date, user_id)
);

