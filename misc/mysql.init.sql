-------------------------------------
-- script to create the rzw database
-- created  2019-07-15
-- modified 2019-10-31
-- by paco pompeani
-------------------------------------

DROP TABLE IF EXISTS rzw.user, rzw.day CASCADE;
DROP DATABASE IF EXISTS rzw;
DROP USER IF EXISTS rzw@localhost;

CREATE DATABASE rzw;

-- PLEASE CHANGE THE PASSWORD
CREATE USER rzw IDENTIFIED BY 'swdfQGST19Dhtedrthre45234525';
GRANT USAGE ON *.* TO rzw@localhost IDENTIFIED BY 'swdfQGST19Dhtedrthre45234525';
GRANT ALL PRIVILEGES ON rzw.* TO rzw@localhost;
FLUSH PRIVILEGES;

USE rzw;

CREATE TABLE user (
	id SMALLINT NOT NULL AUTO_INCREMENT,
	mail VARCHAR(255) NOT NULL UNIQUE,
	passwd VARCHAR(255) NOT NULL,
	color CHAR(6),
	autoplay BOOLEAN,
	PRIMARY KEY (id)
);

CREATE TABLE day (
	date CHAR(10) NOT NULL,
	user_id SMALLINT NOT NULL,
	flow TINYINT,
	pain TINYINT,
	text TEXT,
	PRIMARY KEY (DATE, user_id),
	CONSTRAINT fk_number_one FOREIGN KEY (user_id) REFERENCES user (id)
);

