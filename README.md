# Rule Ze World

rzw est une application web qui permet un suivi simple de vos menstruations. 

*rzw is a web app allowing simple monitoring of your menstruations.*

## Installer sa propre version / Install its own version

### Dépendances / Dependancies
- Un serveur web correctement lié à PHP
- [Node.js](https://nodejs.org) v10.x minimum avec npm (ou yarn)
- PHP v7.x minimum avec le driver PDO sqlite

- *A web server correctly bound to PHP*
- *[Node.js](https://nodejs.org) v10.x minimum with npm (or yarn)*
- *PHP v7.x minimum with the sqlite PDO driver*

### Cloner le projet / Clone the project

```
git clone https://gitlab.com/ppom/rzw
cd rzw
```

### Installer les dépendances / Install dependancies

```
yarn
```
ou
*or*
```
npm install
```

### Créer la base de données / Create the database

```
sqlite3 $DB_PATH < misc/sqlite.init.sql
```

### Transpiler le code Svelte.js en code Vanilla JS / Transpile Svelte.js code into Vanilla JS code

```
yarn build
```
ou
*or*
```
npm run build
```

### Configuration du serveur web / Web server configuration

Tout ce que je peux vous dire, c'est que c'est le dossier rzw/public qui doit être servi. 
Deux variables d'environnement sont lues par PHP :
- `$RZW_DB_FILE` est le chemin vers le fichier SQLite précédemment créé
- `$RZW_ADMIN_PASSWORD_FILE` est le chemin vers un fichier qui contient le mot de passe qui permet de créer un login.
C'est à vous de configurer votre serveur web (nginx, Apache, ...) selon votre cas particulier. 
Mon instance du site était hébergée chez [deblan](https://deblan.io), membre des [CHATONS](https://chatons.org), puis sur mon serveur à l'adresse https://ruleze.world. 
 
*All I can say is that the rzw/public directory should be served. 
Two environnement variables are read by PHP:*
- `$RZW_DB_FILE` *is the path to the previously created SQLite file.*
- `$RZW_ADMIN_PASSWORD_FILE` *is the path to a file containing the password that permits to create a user.*
*It's up to you to configure your web server (nginx, Apache, ...) depending on your personal case. 
My instance of the website was hosted by [deblan](https://deblan.io), member of the [CHATONS](https://chatons.org), then by my homeserver at https://ruleze.world.*

### Protection brute-force avec fail2ban / brute-force protection with fail2ban

Pour protéger une instance d'une attaque brute-force sur la page de connexion, on peut créer un filtre avec fail2ban.
Fail2ban est un outil qui permet de chercher des regex dans des logs applicatifs et bloquer les ainsi IPs détectées.
Après avoir installé et configuré fail2ban, ces deux fichiers permettent de protéger RuleZeWorld.

*To protect an instance for a brute-force attack on the login page, one can create a fail2ban filter.
Fail2ban is a tool to search for regex in app logs and block detected IPs.
When fail2ban is installed and configured, these 2 snippets permit to protect RuleZeWorld:*

create `/etc/fail2ban/filter.d/rzw.conf`:
```toml
[INCLUDES]
before = common.conf

[Definition]
failregex = ^.*\[error\].*FastCGI sent in stderr: "PHP message: .*;(login|create_user);.*" while reading response header from upstream, client: <ADDR>, .* upstream: "fastcgi://unix:/ru
m/rzw.sock:",.*$
ignoreregex =
journalmatch = _SYSTEMD_UNIT=nginx.service
```

add in `/etc/fail2ban/jails.local`:
```toml
[rzw]
enabled = true
port = 80,443
filter = rzw

maxretry = 5
findtime = 3600
bantime = 7200
```

Il faut ensuite :

- Lire vos logs pour adapter la `failregex`, qui peuvent varier selon l'environnement PHP et le serveur web
- Voir des [exemples](https://github.com/fail2ban/fail2ban/tree/master/config/filter.d) en ligne pour adapter `journalmatch`, qui peut varier selon le serveur web utilisé et l'[utilisation ou non](https://github.com/fail2ban/fail2ban/blob/master/config/jail.conf#L128=) de `systemd`/`journald`
- Adapter les valeurs de la *jail* selon le niveau de protection souhaité

*Then you must:*

- *Read your logs to adapt `failregex`, they can change with the PHP environment and web server.*
- *See [examples](https://github.com/fail2ban/fail2ban/tree/master/config/filter.d) to adapt `journalmatch`. It changes with the webserver and the [use or not](https://github.com/fail2ban/fail2ban/blob/master/config/jail.conf#L128=) of `systemd`/`journald`*
- *Adapt the properties of the* jail *according to the desired protection level*

## Contributeurs / Contributors

Ce projet a pour but de rester simple et de s'approcher (modestement) de la [philosophie du groupe suckless](https://suckless.org/philosophy). 
Je l'ai développé seul. Je suis cependant ouvert à toute contribution, notamment concernant :
- la sécurité,
- la simplification du code,
- le design de l'interface,
- l'accessibilité numérique
- l'ajout d'une page d'administration
- la conformité aux normes du web, W3C etc.

*This project aims to stay simple and to (modestly) approach the [suckless philosophy](https://suckless.org/philosophy). 
I developped it alone. I am open to any contribution, in particular in terms of :*
- *security,*
- *code simplification,*
- *user interface design,*
- *accessibility,*
- *adding an admin interface*
- *comformity to web norms, W3C etc.*

## Licence

GPLv3
